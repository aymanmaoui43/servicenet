﻿namespace Front.Entities
{
    public class UserDTO
    {
        public int Id { get; set; }
        public  string Name { get; set; }
        public  string Email { get; set; }
    }

    public class UserLogin
    {
        public  string Name { get; set; }
        public  string Pass { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? PasswordHash { get; set; }

        public override string ToString()
        {
            return $"Id: ${Id} Name: ${Name} Email : ${Email} Pass: ${PasswordHash}";
        }
    }
    public class UserCreateModel
    {
        public required string Password { get; set; }
        public required string Name { get; set; }
        public required string Email { get; set; }
    }

    public class JWTAndUser{
        public required string Token {get; set;}
        public required UserDTO? JWTUser {get; set;}
    }
}
