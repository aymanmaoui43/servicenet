﻿using Microsoft.AspNetCore.Mvc;
using Front.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Security.Cryptography.Xml;
using System.Text.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;


namespace Front.Services
{
    public class TaskService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private ProtectedLocalStorage _sessionStorage;

        public TaskService(IHttpClientFactory httpClientFactory, ProtectedLocalStorage sessionStorage)
        {
            _httpClientFactory = httpClientFactory;
            _sessionStorage = sessionStorage;
        }



        public async Task<TaskDTO> CreateTaskAsync(string Text, bool isDone)
        {
            TaskCreate model = new TaskCreate { Text = Text, IsDone = isDone };
            using (var client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:5000/");
                //HttpResponseMessage response = await client.PostAsJsonAsync("api/Task", model);
                var jwt = await _sessionStorage.GetAsync<string>("jwt");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt.Value);
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Task", model);

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    var result = await response.Content.ReadFromJsonAsync<TaskDTO>();
                    return result;

                }
                else
                {
                    // Log or handle non-OK responses
                    Console.WriteLine("Non-OK Status Code: " + response.StatusCode);
                    Console.WriteLine("Response Content: " + await response.Content.ReadAsStringAsync());
                    return null;
                }
            }
        }



        public async Task<IEnumerable<Front.Entities.TaskDTO>> GetTaskAsync()
        {
            using (var client = _httpClientFactory.CreateClient())
            {

                client.BaseAddress = new System.Uri("http://localhost:5000/");
                try
                {
                    var jwt = await _sessionStorage.GetAsync<string>("jwt");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt.Value);
                    HttpResponseMessage response = await client.GetAsync("api/Task");
                    //HttpResponseMessage response = await client.GetAsync("api/Task");


                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadFromJsonAsync<IEnumerable<Front.Entities.TaskDTO>>();
                        return result;
                    }
                    else
                    {
                        // Log or handle different HTTP status codes here
                        return null;
                    }
                }
                catch (HttpRequestException ex)
                {
                    // Log or handle the exception
                    return null;
                }
            }
        }



        public class TaskDTO
        {
            public int Id { get; set; }
            public string Text { get; set; }
            public bool IsDone { get; set; }
        }

    }
}

