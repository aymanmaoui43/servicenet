﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Net;
using TaskService.Entities;
using System.Runtime.CompilerServices;
using System.Reflection;
using GatewayService.Entities;
using Microsoft.AspNetCore.Authorization;

namespace GatewayService.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public TaskController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateTask(TaskCreate model)
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
                if (UserId == null) return Unauthorized();

                client.BaseAddress = new System.Uri("http://localhost:5002/");
                HttpResponseMessage response = await client.PostAsJsonAsync($"api/Tasks/{UserId}", model);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadFromJsonAsync<TaskService.Entities.TaskDTO>();
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Unable to create");
                }
            }
        }
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, TaskCreate model)
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
                if (UserId == null) return Unauthorized();

                client.BaseAddress = new System.Uri("http://localhost:5002/");
                HttpResponseMessage response = await client.PutAsJsonAsync($"api/Tasks/{UserId}/{id}", model);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadFromJsonAsync<TaskService.Entities.TaskDTO>();
                    return Ok(result);
                }
                else
                {
                    
                    return BadRequest("Unable to update");
                }
            }
        }
        /*
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;
                if (UserId == null) return Unauthorized();

                client.BaseAddress = new System.Uri("http://localhost:5002/");
                HttpResponseMessage response = await client.GetAsync($"api/Tasks/{id}");
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadFromJsonAsync<TaskService.Entities.TaskDTO>();
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Unable to fetch");
                }
            }
        }*/

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<TaskService.Entities.Task>>> Get()
        {
            using (var client = _httpClientFactory.CreateClient())
            {
                var UserId = User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value;


                if (UserId == null) return Unauthorized();

                client.BaseAddress = new System.Uri("http://localhost:5002/");
                HttpResponseMessage response = await client.GetAsync($"api/Tasks/{UserId}");

                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadFromJsonAsync<IEnumerable<TaskService.Entities.TaskDTO>>();
                    return Ok(result);
                }
                else
                {
                    var resp =await  response.Content.ReadAsStringAsync();
                    Console.WriteLine($"ereeur; {resp}");
                    //Console.WriteLine(response.Content);
                    return BadRequest("Unable to fetch");
                }
            }
        }

    }
}

